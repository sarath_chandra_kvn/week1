package VehicleRepair;

/**
 * @author Sarath.x.kappagantula@kp.org
 */
public class RepairCenter extends Customer{

    String jobStatus;
    RepairCenter(){}

    RepairCenter(String customerID,String customerName,String dateOfJoining,String jobStatus ) {
        super( customerID, customerName, dateOfJoining);
        this.jobStatus =jobStatus;
    }

    void dailyIncomeReport()
    {
        System.out.println("Daily Income Report");
    }

    void getInventoryStatus(){
        System.out.println("Get Inventory Status");
    }

    String getJobStatus()
    {
        return jobStatus;
    }

    void getJobStatus(String jobStatus)
    {
        this.jobStatus = jobStatus;
    }

    public static void main(String s[])
    {
        RepairCenter customer1 = new RepairCenter("123456","Steve","2018-07-01","Pending");
        System.out.println(customer1.isALoyalCustomer());
        customer1.getInventoryStatus();
        customer1.getJobStatus();

        RepairCenter center = new RepairCenter();
        center.dailyIncomeReport();
    }
}
