package VehicleRepair;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * @author Sarath.x.kappagantula@kp.org
 */
public class Customer {

    String customerID;
    String customerName;
    Date dateOfJoining;


    Customer() {
    }

    Customer(String customerID, String customerName, String dateOfJoining) {
        this.customerID = customerID;
        this.customerName = customerName;
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd"); // yyyy-MM-dd
        try {
            this.dateOfJoining = ft.parse(dateOfJoining);
        } catch (ParseException ex) {
        }

    }

    boolean isALoyalCustomer() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date today = new Date(System.currentTimeMillis());
        try {
            today = formatter.parse("yyyy-MM-dd");

        } catch (ParseException ex) {
        }
        Calendar a = getCalendar(dateOfJoining);
        Calendar b = getCalendar(today);

        return b.get(Calendar.YEAR) - a.get(Calendar.YEAR) >= 2;

    }


    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }

}