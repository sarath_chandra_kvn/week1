package VehicleEntities;

/**
 * @author Sarath.x.kappagantula@kp.org
 */
public class Vehicle
{
   String name;
   int cost;
   String fuel;
   int max_Speed;

   Vehicle() {}
   Vehicle(String name, int cost,String fuel, int max_Speed)
   {
       this.name = name;
       this.cost=cost;
       this.fuel=fuel;
       this.max_Speed = max_Speed;

   }

    public void start()
    {
        System.out.println("Start");
    }

    public void stop()
    {
        System.out.println("Stop");
    }

    public void move()
    {
        this.start();
        System.out.println("Move");
    }

    public void accelerate()
    {
        System.out.println("Accelerate");
    }

    public void brake()
    {
        System.out.println("Brake");
        stop();

    }
}
